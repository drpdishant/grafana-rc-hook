function hook(request) {

        var alertColor = "warning";
        var emoji = ":warning:"
//         :warning:  > aa emoji warning ma
// :exclamation: > aa emoji alerting ma
// :white_check_mark:  > aa emoji back to normal ma

        if (request.body.state == "ok") {
            alertColor = "good";
            emoji = ":white_check_mark:"
        } else if (request.body.state == "firing") {
            alertColor = "danger";
            emoji = ":fire:"
        }
        // console.log(request.body);

        let finFields = [];
        for (i = 0; i < request.body.evalMatches.length; i++) {
            var endVal = request.body.evalMatches[i];
            var elem = {
                title: endVal.metric,
                value: endVal.value,
                short: false
            };

            finFields.push(elem);
        }



        return {
            content: {
              alias: "Grafana",
              emoji: emoji,
              text: request.body.message,
              "attachments": [
                {
                  "title": request.body.title,
                  "title_link": request.body.ruleUrl,
                  "text": request.body.ruleName,
                  "image_url": request.body.imageUrl,
                  "fields": finFields,
                  "color": alertColor
                }
              ]
            }
        };

        return {
            error: {
                success: false
            }
        };
}

module.exports = { hook }