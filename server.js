const express = require("express")
const bodyParser = require("body-parser")
const hooks = require('./hook'); 
// Initialize express and define a port
const app = express()
const PORT = 8080

// Tell express to use body-parser's JSON parsing
app.use(bodyParser.json())

// Start express on the defined port
app.listen(PORT, () => console.log(`🚀 Server running on port ${PORT}`))

app.get("/", (req, res) => {
  console.log(req.body) // Call your action on the request here
  res.send('GET request to the demo webhook server')
  res.status(200).end() // Responding is important
})

app.post("/hook", (req, res) => {
   console.log("%j", req.body)
   var data = hooks.hook(req)
   console.log(data)
   console.log(data.content.attachments[0])
   // Call your action on the request here
  res.status(200).end() // Responding is important
})