# Simple Webhook Script for Rocketchat to Receive and Parse Grafana Alerts

## Usage

- Copy the contents of [**`rc.js`**](./rc.js) to Scripts Section of [Rocketchat Incoming Webhook](https://docs.rocket.chat/guides/administrator-guides/integrations#incoming-webhook-script).
- Configure Grafana Notification Rule to webhook type and set the generated incoming webhook url in the url field.
